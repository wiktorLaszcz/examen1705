<?php

namespace App\Http\Controllers;
use App\Exam;
use Illuminate\Http\Request;
use App\Policies\ExamPolicy;


class ExamController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('index', Exam::class);
        $exams = Exam::paginate(5);
        return view('exam.index',["exams" => $exams]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $exam = Exam::find($id);
        $this->authorize('delete', $exam);

        $exam->delete();

        return redirect("/exams");
      }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function saveSession($id)
    {
        $exam = Exam::find($id);
         session(['examsSession'=>$exam]);

         return redirect("/exams");
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dropSession()
    {


         session()->forget('examsSession');
            session()->flush();

         return redirect("/exams");
    }


}
