<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function exams()
    {
        return $this->belongsToMany('App\Exam');
    }

    public function modules()
    {
        return $this->hasMany('App\Module');
    }
}
