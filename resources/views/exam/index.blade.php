@extends('layouts.app')

@section('content')


<div class="container">
<h1>List</h1>
@auth
<table style="border: 2px #99F solid;">
    <tr>
        <th style="border: 2px #AAF solid;">Titulo</th>
        <!--<th style="border: 2px #AAF solid;">Description</th> -->
        <th style="border: 2px #AAF solid;">Modulo</th>
        <th style="border: 2px #AAF solid;">Propietario</th>
    </tr>

    @foreach($exams as $exam)
    <tr style="border: 2px #AAF solid;">

        <td style="border: 2px #AAF solid;">{{ $exam->title }}</td>
        <td style="border: 2px #AAF solid;">{{ $exam->user->name }}</td>
        <td style="border: 2px #AAF solid;">

            {{ $exam->modules }} ,

        </td>
        <td>
            <a href="/exams/{{ $exam->id }}/remember"><input type="button" value="Guardar en sesion"></a>
        </td>
        @can('show',$exam)
        <td>
            <form method="post" action="/exams/{{ $exam->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="delete">

                <input type="submit" value="Eliminar">
            </form>
        </td>
        @endcan
        </td>
    </tr>
    @endforeach
</table>
{{ $exams->links() }}
@endauth
</div>

@endsection
