@extends('layouts.app')

@section('content')


<div class="container">
<h1>List</h1>
<table style="border: 2px #99F solid;">
    <tr>
        <th style="border: 2px #AAF solid;">Titulo</th>
    </tr>

    @foreach($questions as $question)
    <tr style="border: 2px #AAF solid;">

        <td style="border: 2px #AAF solid;">{{ $question->text }}</td>
        @if($question->answer == 'a')
        <td style="border: 2px #AAF solid;" class="bg-success">{{ $question->a }}</td>
        @else
        <td style="border: 2px #AAF solid;">{{ $question->a }}</td>
        @endif
        @if($question->answer == 'b')
        <td style="border: 2px #AAF solid;" class="bg-success">{{ $question->b }}</td>
        @else
        <td style="border: 2px #AAF solid;">{{ $question->b }}</td>
        @endif
       @if($question->answer == 'c')
        <td style="border: 2px #AAF solid;" class="bg-success">{{ $question->c }}</td>
        @else
        <td style="border: 2px #AAF solid;">{{ $question->c }}</td>
        @endif
        @if($question->answer == 'd')
        <td style="border: 2px #AAF solid;" class="bg-success">{{ $question->d }}</td>
        @else
        <td style="border: 2px #AAF solid;">{{ $question->d }}</td>
        @endif
    </tr>
    @endforeach
</table>
</div>

@endsection
